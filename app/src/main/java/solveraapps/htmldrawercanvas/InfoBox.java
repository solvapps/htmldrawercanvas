package solveraapps.htmldrawercanvas;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreas on 12.08.2017.
 */

public class InfoBox {

    String text="";
    Context context;

    String title="";

    List<InfoProp> props=new ArrayList<InfoProp>();

    private class InfoProp {

       String property="";
        Object value="";
    }

    public void addProp(String property, Object value){
    }

    public InfoBox(Context context){
    }

    public void draw(){
        // Zeichne Titel zuerst
    }

    //TODO: Create a method, that draws HTML Text into Canvas.
    public void drawHtml(Canvas canvas, String htmltext, int top, int left, int bottom, int rigth){

        LinearLayout layout = new LinearLayout(context);
        TextView textView = new TextView(context);
        textView.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.height = bottom - top;
        params.width = rigth - left;
        textView.setLayoutParams(params);
        textView.setText(Html.fromHtml(htmltext));
        layout.addView(textView);
        layout.measure(canvas.getWidth(), canvas.getHeight());
        layout.layout(100, 300, canvas.getWidth(), canvas.getHeight());
        canvas.translate(left, top);
        layout.draw(canvas);
        canvas.restore();

    }


}
