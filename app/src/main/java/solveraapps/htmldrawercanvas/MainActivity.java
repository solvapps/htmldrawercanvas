package solveraapps.htmldrawercanvas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    SimpleView simpleview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        simpleview = new SimpleView(this,null);

        LinearLayout v = (LinearLayout) findViewById(R.id.linearLayout);
        v.addView(simpleview);

        simpleview.invalidate();

    }
}
