package solveraapps.htmldrawercanvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

/**
 * Created by andreas on 10.08.2017.
 */

public class SimpleView extends View {

    Paint paint = new Paint();
    TextPaint textpaint = new TextPaint();
    Context context;
    public SimpleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(Color.YELLOW);
        canvas.drawCircle(200, 200, 100, paint);

        //TODO:
        String htmltext = "<h2>Hello this is a html text.</h2>\n" +
                "<p>I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.<br />I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.<br />I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.<br />I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.<br />I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.<br />I can have multiple lines and <span style=\"color: #ff0000;\"><strong>HTML</strong> </span>formatting tags.</p>";
        drawHtml(canvas,htmltext,200,400,600,800);


    }
    //TODO: Create a method, that draws HTML Text into Canvas.
    public void drawHtml(Canvas canvas,String htmltext, int top, int left, int bottom, int rigth){

        LinearLayout layout = new LinearLayout(context);
        TextView textView = new TextView(context);
        textView.setVisibility(View.VISIBLE);
        LayoutParams params = new LayoutParams(0, LayoutParams.WRAP_CONTENT);
        params.height = bottom - top;
        params.width = rigth - left;
        textView.setLayoutParams(params);
        textView.setText(Html.fromHtml(htmltext));
        layout.addView(textView);
        layout.measure(canvas.getWidth(), canvas.getHeight());
        layout.layout(100, 300, canvas.getWidth(), canvas.getHeight());
        canvas.translate(left, top);
        layout.draw(canvas);
        canvas.restore();

    }
}